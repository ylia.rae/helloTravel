from flask import Flask, g, flash, render_template, redirect, url_for, request
from wtforms import Form, TextField, TextAreaField, IntegerField, validators, StringField, SubmitField, ValidationError
from wtforms.validators import InputRequired
from databaseModel import db, Country, City, Food, Attraction, Foodsoc, Attrsoc
from objectsModel import FoodObj, AttrObj, CityObj
from forms import CountryForm, CityForm, FoodForm, AttrForm
from jinja2 import Environment, PackageLoader
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, func
from sqlalchemy.sql import select
from collections import defaultdict

import create as dbInsertHandler
import delete as dbDeleteHandler

app = Flask(__name__)

env = Environment(loader = PackageLoader('main','templates'))

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/travelApp.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'BM~xfdtxddyxf6xe1x94^xebxd40xf4+43Xxadx12:xa0xd2'

db.init_app(app)

mealTypeList= [('undecided', 'Undecided'), ('breakfast', 'Breakfast'), ('lunch', 'Lunch'), ('dinner', 'Dinner'), ('dessert', 'Dessert'), ('snack', 'Snack'), ('cafe', 'Cafe')]
attrTypeList = [('undesignated', 'Undesignated'), ('monument', 'Monument'), ('museum', 'Museum'), ('park', 'Park'), ('shop', 'Shop'), ('themepark', 'Theme Park'), ('historical', 'Historical Place'), ('nature', 'Nature')]
interestLevelList = ["Meh", "Would be nice", "Won\'t be heartbroken if I didn\'t go", "I\'d be really sad if I don\'t go, but I\'ll survive", "I MUST GO"]

### Helper Methods ###
def getAllCountries():
    return Country.query.order_by(Country.name).all()
### End Helper Methods ###

@app.context_processor
def context_processor():
    countryForm = CountryForm(request.form)
    foodForm = FoodForm(request.form)
    cityForm = CityForm(request.form)
    attrForm = AttrForm(request.form)
    return dict(countries=getAllCountries(), countryForm=countryForm, cityForm=cityForm, foodForm=foodForm, attrForm=attrForm, mealTypeList=mealTypeList, attrTypeList=attrTypeList, interestLevelList=interestLevelList)

### Sidebar Country List ###
@app.route('/', methods=['POST', 'GET'])
def home():
    countryform = CountryForm(request.form)
    if request.method=='GET':
        return render_template('index.html')
    else:
        return render_template('blank.html')
### End Sidebar Country List ###

### Clicking on a country in the sidebar list ###
@app.route('/country/<activeCountry>', methods=['GET'])
def country(activeCountry):
    foodObjList = []
    foodList = []

    attrObjList = []
    attrList = []

    activeCountryObj=Country.query.filter_by(name=activeCountry).first()

    # Getting food from each city in the country
    for city in activeCountryObj.countryCities:
        if Food.query.filter(Food.cities.any(cityID=city.id)).all() != []: 
            foodList.append(Food.query.filter(Food.cities.any(cityID=city.id)).all()) #all the foods in active country
        if Attraction.query.filter(Attraction.cities.any(cityID=city.id)).all() != []: 
            attrObjList.append(Attraction.query.filter(Attraction.cities.any(cityID=city.id)).all()) #all the attractions in active country    

    # get addresses from food           
    if foodList:
        for foodItem in foodList:
            for data in foodItem:
                foodItemAddr = Foodsoc.query.filter(Foodsoc.foodID == data.id).all()
                for item in foodItemAddr:
                    foodObjList.append(FoodObj(data.name, item))

    for data in attrObjList:
        for attrItem in data:
            attrList.append(attrItem)

    activeItem = ["country", activeCountryObj]

    return render_template('country.html', foodObjList=foodObjList, attrList=attrList, activeItem=activeItem)
### End Clicking on a country in the sidebar list ###

### Clicking on a city in the sidebar list or in the country page ###
@app.route('/city/')
@app.route('/city/<activeCityId>', methods=['GET'])
def city(activeCityId):
    foodList = []
    foodObjList = []
    attrObjList = []
    attrList = []

    activeCityObj=City.query.filter_by(id=activeCityId).first()
    foodList.append(Food.query.filter(Food.cities.any(cityID=activeCityId)).all())
    attrObjList.append(Attraction.query.filter(Attraction.cities.any(cityID=activeCityId)).all())

    # get addresses from food           
    if foodList:
        for foodItem in foodList:
            for data in foodItem:
                foodItemAddr = Foodsoc.query.filter(Foodsoc.foodID == data.id).all()
                for item in foodItemAddr:
                    foodObjList.append(FoodObj(data.name, item))

    for data in attrObjList:
        for attrItem in data:
            attrList.append(attrItem)

    countryName = Country.query.filter(Country.id == activeCityObj.countryID).first().name
    activeItem = ["city", activeCityObj, countryName]

    return render_template('city.html', foodObjList=foodObjList, attrList=attrList, activeItem=activeItem)
### End Clicking on a city in the sidebar list or in the country page ###

### Form Handling ###
@app.route('/addCountry', methods=['GET', 'POST'])
def addCountry():
    form = CountryForm(request.form)
    country = None
    if request.method == 'POST':
        if form.validate() == False:
            flash("All fields are required.")
        else:
            country=request.form['newCountry']
            dbInsertHandler.insertCountry(country);
    
    return redirect(url_for('country', activeCountry=country))         

@app.route('/addTripModal', methods=['GET', 'POST'])
def addTripModal():
    countryform = CountryForm(request.form)
    return render_template('addTripModal.html')

@app.route('/addCity/<activeCountry>', methods=['POST'])
def addCityModal(activeCountry):
    form = CityForm(request.form)
    city = request.form.getlist('newCity[]')

    for item in city:
       dbInsertHandler.insertCity(item, activeCountry)

    return render_template('country.html', activeCountry=activeCountry) 

@app.route('/addFoodModal/<activeCountry>', methods=['POST', 'GET'])
def addFoodModal(activeCountry):
    form = FoodForm(request.form)
   
    if request.method == 'POST':
        if form.validate() == False:
            print("ValidationError")
            flash("All fields are required.")
        else:
            name=request.form['name']
            city=request.form['radioValue']
            address=request.form['address']
            mealType=request.form['mealType']
            interestLevel=request.form['interestLevel']
            notes=request.form['foodNotes']

            dbInsertHandler.insertFood(name, city, address, mealType, interestLevel, notes);

    activeItem = ["country", activeCountryObj]

    return render_template('country.html', activeCountry=activeCountry)

@app.route('/getFoodData/<int:selectedFood>', methods=['GET'])
def getFood(selectedFood):
    if request.method == 'GET':
        selectedFoodData = Foodsoc.query.filter(Foodsoc.id == selectedFood).all()
        foodName = Food.query.filter_by(id=(Foodsoc.query.filter(Foodsoc.id == selectedFood).first().foodID)).first().name
        foodData = FoodObj(foodName, selectedFoodData[0]) 
        activeItem = ["food", foodData]
        selectedCity = City.query.get(foodData.city)
        activeCountry = Country.query.filter_by(id = selectedCity.countryID).first().name

        foodForm = FoodForm(obj=foodData)

    return render_template('editFood.html', foodForm=foodForm, selectedCity=selectedCity, activeCountry=activeCountry, activeItem=activeItem)

@app.route('/updateFood/<assocID>', methods=['POST', 'GET'])
def updateFood(assocID):
    form = FoodForm(request.form)
    if request.method == 'POST':
        if form.validate() == False:
            print("ValidationError")
            flash("All fields are required.")
        else:
            newAddress=request.form['newAddress']
            newMealType=request.form['newMealType']
            newInterestLevel=request.form['newInterestLevel']
            newNotes=request.form['newNotes']

            food = Foodsoc.query.filter_by(id=assocID).first()

            food.mealType = newMealType
            food.address = newAddress
            food.interestLevel = newInterestLevel
            food.notes = newNotes

            db.session.commit()

    selectedCity = City.query.get(food.cityID)
    activeCountry = Country.query.filter_by(id = selectedCity.countryID).first().name
           
    return redirect(url_for('country', activeCountry=activeCountry))

@app.route('/addAttraction/<activeCountry>', methods=['POST', 'GET'])
def addAttraction(activeCountry):
    form = AttrForm(request.form)
    if request.method == 'POST':
        if form.validate() == False:
            flash("All fields are required.")
        else:
            name=request.form['name']
            city=request.form['radioValue']
            address=request.form['address']
            attrType=request.form['attrType']
            interestLevel=request.form['interestLevel']
            notes=request.form['attractionNotes']

            dbInsertHandler.insertAttraction(name, city, address, attrType, interestLevel, notes);

    return redirect(url_for('country', activeCountry=activeCountry)) 

@app.route('/getAttrData/<int:selectedAttr>', methods=['GET'])
def getAttrData(selectedAttr):
    if request.method == 'GET':
        selectedAttrData = Attrsoc.query.filter(Attrsoc.id == selectedAttr).all()
        attrName = Attraction.query.filter_by(id=(Attrsoc.query.filter(Attrsoc.id == selectedAttr).first().attractionID)).first().name
        attrData = AttrObj(attrName, selectedAttrData[0]) 
        activeItem = ["attraction", attrData]
        selectedCity = City.query.get(attrData.city)
        activeCountry = Country.query.filter_by(id = selectedCity.countryID).first().name

        attrForm = AttrForm(obj=attrData)

    return render_template('editAttraction.html', attrForm=attrForm, selectedCity=selectedCity, activeCountry=activeCountry, activeItem=activeItem)

@app.route('/updateAttraction/<assocID>', methods=['POST', 'GET'])
def updateAttraction(assocID):
    form = AttrForm(request.form)
    if request.method == 'POST':
        if form.validate() == False:
            print("ValidationError")
            flash("All fields are required.")
        else:
            newAddress=request.form['newAddress']
            newAttrType=request.form['newAttrType']
            newInterestLevel=request.form['newInterestLevel']
            newNotes=request.form['newNotes']

            attr = Attrsoc.query.filter_by(id=assocID).first()

            attr.attrType = newAttrType
            attr.address = newAddress
            attr.interestLevel = newInterestLevel
            attr.notes = newNotes

            db.session.commit()
           
    selectedCity = City.query.get(attr.cityID)
    activeCountry = Country.query.filter_by(id = selectedCity.countryID).first().name

    return redirect(url_for('country', activeCountry=activeCountry))

@app.route('/deleteItem/<category>/<itemName>/<idDelete>', methods=['POST', 'GET'])
def deleteItem(category, itemName, idDelete):
    if category == 'food':
        dbDeleteHandler.deleteFood(idDelete)

    if category == 'attraction':
        dbDeleteHandler.deleteAttraction(idDelete)

    if category == 'city':
        dbDeleteHandler.deleteCity(idDelete)

    if category == 'country':
        dbDeleteHandler.deleteCountry(idDelete)

    return redirect(url_for('index.html'))
 
### End Form Handling ###

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')