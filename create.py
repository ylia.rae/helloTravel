from databaseModel import Country
from databaseModel import City
from databaseModel import Food
from databaseModel import Attraction
import sqlite3 as sql

def insertCountry(country):
    con = sql.connect("database/travelApp.db")
    cur = con.cursor()
    cur.execute("INSERT INTO Country (Name) VALUES (?)", [country])
    con.commit()
    con.close()

def insertCity(city, country):
    con = sql.connect("database/travelApp.db")
    cur = con.cursor()
    countryID = Country.query.filter_by(name=country).first()
    cur.execute("INSERT INTO City (Name, countryID) VALUES (?,?)", (city, countryID.id))
    con.commit()
    con.close()

def insertFood(foodName, city, address, mealType, interestLevel, notes):
    con = sql.connect("database/travelApp.db")
    cur = con.cursor()
    cityID = City.query.filter_by(name=city).first()
    food = Food.query.filter_by(name=foodName).count()
    if food == 0:
        cur.execute("INSERT INTO Food (Name) VALUES (?)", [foodName])
        con.commit()
    
    foodID = Food.query.filter_by(name=foodName).first()
    cur.execute("INSERT INTO Foodsoc (CityID, FoodID, Address,  MealType, InterestLevel, Notes) VALUES (?,?,?,?,?,?)", (cityID.id, foodID.id, address, mealType, interestLevel, notes))
    con.commit()
    con.close()

def insertAttraction(attrName, city, address, attrType, interestLevel, notes):
    con = sql.connect("database/travelApp.db")
    cur = con.cursor()
    cityID = City.query.filter_by(name=city).first()
    attr = Attraction.query.filter_by(name=attrName).count()
    if attr == 0:
        cur.execute("INSERT INTO Attraction (Name) VALUES (?)", [attrName])
        con.commit()

    attrID = Attraction.query.filter_by(name=attrName).first()
    cur.execute("INSERT INTO Attrsoc (CityID, AttractionID, Address,  AttrType, InterestLevel, Notes) VALUES (?,?,?,?,?,?)", (cityID.id, attrID.id, address, attrType, interestLevel, notes))
    con.commit()
    con.close()