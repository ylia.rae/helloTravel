from databaseModel import db, Country, City, Food, Attraction, Foodsoc, Attrsoc
import sqlite3 as sql

def deleteFood(idDelete):
    foodId = Food.query.filter_by(id=(Foodsoc.query.filter(Foodsoc.id == idDelete).first().foodID)).first().id
    Food.query.filter_by(id=foodId).delete()
    db.session.commit()
    Foodsoc.query.filter_by(id=idDelete).delete()
    db.session.commit()
    db.session.close()

def deleteAttraction(idDelete):
    attrId = Food.query.filter_by(id=(Attrsoc.query.filter(Attrsoc.id == idDelete).first().attractionID)).first().id
    Attraction.query.filter_by(id=attrId).delete()
    db.session.commit()
    Attrsoc.query.filter_by(id=idDelete).delete()
    db.session.commit()
    db.session.close()

def deleteCity(idDelete):
    #get all ids of Foodsoc in city
    foodSocIdAll = Foodsoc.query.filter(Foodsoc.cityID == idDelete).all()
    if foodSocIdAll:
        for foodSocId in foodSocIdAll:
            deleteFood(foodSocId.id)
    db.session.commit()
    #get all ids of Attrsoc in city
    attrSocIdAll = Attrsoc.query.filter(Attrsoc.cityID == idDelete).all()
    if attrSocIdAll:
        for attrSocId in attrSocIdAll:
            deleteAttraction(attrSocId.id)
    db.session.commit()
    #delete city
    City.query.filter_by(id=idDelete).delete()
    db.session.commit()
    db.session.close()

def deleteCountry(idDelete):
 	#get all ids of city
    cityIdAll = City.query.filter(City.countryID == idDelete).all()
	#get all ids of Foodsoc in city
    if cityIdAll:
        for cityID in cityIdAll:
            deleteCity(cityID.id)

    Country.query.filter_by(id=idDelete).delete()
    db.session.commit()
    db.session.close()