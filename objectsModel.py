from databaseModel import db, Country, City, Food, Attraction, Foodsoc, Attrsoc

class FoodObj:
    def __init__(self, name, food):
        self.id = food.id #assocID
        self.name = name
        self.address = food.address
        self.city = food.cityID
        self.mealType = food.mealType
        self.interestLevel = food.interestLevel
        self.notes = food.notes

class AttrObj:
    def __init__(self, name, attraction):
        self.id = attraction.id #assocID
        self.name = name
        self.address = attraction.address
        self.city = attraction.cityID
        self.attrType = attraction.attrType
        self.interestLevel = attraction.interestLevel
        self.notes = attraction.notes

class CityObj:
	def __init__(self, city):
		self.id = city.id
		self.name = city.name

class CountryObj:
	def __init__(self, country):
		self.id = country.id
		self.name = country.name