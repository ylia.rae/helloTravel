import sqlite3 as sql
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    countryCities = db.relationship('City', back_populates='country', order_by='City.name')

class Foodsoc(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cityID = db.Column(db.Integer, db.ForeignKey('city.id'))
    foodID = db.Column(db.Integer, db.ForeignKey('food.id'))
    address = db.Column(db.String(50));
    interestLevel = db.Column(db.String(50))
    mealType = db.Column(db.String(10))
    notes = db.Column(db.String(500))
    food = db.relationship("Food", back_populates="cities")
    cityFood = db.relationship("City", back_populates="foods")

class Attrsoc(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cityID = db.Column(db.Integer, db.ForeignKey('city.id'))
    attractionID = db.Column(db.Integer, db.ForeignKey('attraction.id'))
    address = db.Column(db.String(50));
    interestLevel = db.Column(db.String(50))
    attrType = db.Column(db.String(10))
    notes = db.Column(db.String(500))
    attraction = db.relationship("Attraction", back_populates="cities")
    cityAttr = db.relationship("City", back_populates="attractions")
    
class City(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    countryID = db.Column(db.Integer, db.ForeignKey('country.id'))
    country = db.relationship("Country", back_populates="countryCities")
    foods = db.relationship("Foodsoc", back_populates="cityFood")
    attractions = db.relationship('Attrsoc', back_populates="cityAttr")

class Food(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    cities = db.relationship("Foodsoc", back_populates="food")

class Attraction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    cities = db.relationship("Attrsoc", back_populates="attraction")

