#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask

app = Flask(__name__,
             template_folder='view/templates',
             static_folder='view/static')

app.config['SQLALCHEMY_DATABASE_URI'] = 'database/travelApp.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

env = Environment(loader = PackageLoader('main','templates'))
