from wtforms import Form, TextField, TextAreaField, IntegerField, validators, StringField, SubmitField, ValidationError
from wtforms.validators import InputRequired

class CountryForm(Form):
    newCountry = StringField('Country:', [validators.Required("Please enter a country")])

class CityForm(Form):
    city = StringField('City:', [validators.Required("Please enter a city")])

class FoodForm(Form):
    name = StringField('Name:', [validators.Required("Please enter the location name")])
    address = StringField('Address:')
    city = StringField('City:')
    mealType = StringField('Meal Type:')
    interestLevel = IntegerField('Interest Level:')
    notes = TextAreaField('Notes:')

class AttrForm(Form):
    name = StringField('Name:', [validators.Required("Please enter the location name")])
    address = StringField('Address:')
    city = StringField('City:')
    attrType = StringField('Attraction Type:')
    interestLevel = IntegerField('Interest Level:')
    notes = TextAreaField('Notes:')